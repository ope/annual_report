Documentation : https://ope.gricad-pages.univ-grenoble-alpes.fr/annual_report

Automatisation des figures du rapport ANDRA
===========================================

Ces ensembles de script ont pour fonction la création de figures standardisées pour le
site de l'OPE depuis la base de données de chimie atmosphérique mesuré à l'IGE.

Il est donc nécessaire d'avoir cette base de donnée en format excel disponible.

Examples
========

Depuis le répertoire où est placé `pyANDRA.py`, et avec un fichier `BdD_ANDRA.xlsx`
posédant deux feuilles "ANDRA-PM10" et "ANDRA-PM2.5":

```python
import pyANDRA

dfheader, dfblank, dfvalue = pyANDRA.get_data_from_excel("./BdD_ANDRA.xlsx")

pyANDRA.plot_correlation(dfvalue, pyANDRA.ETMS)
```

Génère la figure suivante

![img ETM PM10](./figures/correlation_ETM_PM10.png)

Dépendances
===========

Il est nécessaire d'installer un paquet qui n'est pas usuel, qui sert à faire du formatage
de données de chimie atmosphérique `py4pm`.

Le plus simple est donc d'installer avec [pip](https://pypi.org/project/pip/) les
dépendances :

    pip install -r requirements.txt


