Automatic figures generation
============================

Installation
------------

All the code lives at : https://gricad-gitlab.univ-grenoble-alpes.fr/ope/annual_report/

Either clone the repository or copy both the `requirements.txt` and `pyANDRA.py` files.

Install the python dependancies running:

.. code-block:: bash

   pip install -r requirements.txt


How to
------

First of all, check that the `DATE_MIN` and `DATE_MAX` values are correct within the
script!


By default, the database is looked for at the same directory as the pyANDRA module, with a
name `BdD_ANDRA.xlsx`. Update it to your need.

If you run the script or call the `plot_all` function, it will produce lots of figures
needed for the report.

.. code-block:: python

   import pyANDRA

   # dfh : headers
   # dfb : blanks
   # df : samples
   dfh, dfb, df = pyANDRA.get_data_from_excel("./BdD_ANDRA.xlsx")

   # plot all figures, and save them as png in the figures/ directory
   pyANDRA.plot_all(df, dfb, fractions=["PM10", "PM2.5"], savefig=True)

But you can also call each function independantly, for instance:

.. code-block:: python

   import pyANDRA

   dfh, dfb, df = pyANDRA.get_data_from_excel("./BdD_ANDRA.xlsx")
   pyANDRA.plot_timeseries(data=df, species=["OC"], fraction="PM10")

produces the following figure:

.. image:: ../figures/timeserie_OC_PM10.png
   :width: 800px
   :align: center


Utilities
---------

All plot functions has a `savefig` (default to False) argument that saves the figure as
png in the `figures/` folder.


Examples
--------

For all these example, we assume that you have already ran the following code (load module
and data). Adapt it to the correct path for the database excel sheet, default one is
`./BdD_ANDRA.xlsx`.

.. code-block:: python

   import pyANDRA

   dfh, dfb, df = pyANDRA.get_data_from_excel(pyANDRA.DB_EXCEL)

Timeseries
~~~~~~~~~~

Default is the PM10 fraction. Change it with the option `fraction="PM2.5"` if needed.

One specie only:

.. code-block:: python

   pyANDRA.plot_timeseries(data=df, species=["OC"])

.. image:: ../figures/timeserie_OC_PM10.png
   :width: 800px
   :align: center


Several species (subplots is optional and plot each species in an independant subplot) :

.. code-block:: python

   pyANDRA.plot_timeseries(data=df, species=["NO3-", "SO42-", "NH4+"], subplots=True)

.. image:: ../figures/timeserie_NO3--SO42--NH4+_subploted_PM10.png
   :width: 800px
   :align: center


Timeseries ratio PM10/PM2.5 for one species
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The default is to take the monthly mean of the specie for PM10 and PM2.5, and compute the
ratio.

.. code-block:: python

   pyANDRA.plot_ratio_PM10_PM25(data=df, sp="OC")

.. image:: ../figures/timeserie_ratio_OC_PM10_PM25.png
   :width: 800px
   :align: center

Timeseries ratio 2 species
~~~~~~~~~~~~~~~~~~~~~~~~~~

The default is to compute the ratio of the 2 species for each date, and each size fraction
given. But it is also possible to aggregate it by monthly mean or other.

.. code-block:: python

   pyANDRA.plot_timeseries_ratio(data=df, sp1="EC", sp2="OC", fractions=["PM10"])

.. image:: ../figures/timeserie_ratio_EC-OC_PM10.png
   :width: 800px
   :align: center

Annual mass balance
~~~~~~~~~~~~~~~~~~~

The mass balance is computed according to:

.. math::

   PM = [OM] + [EC] + [sea~salt] + [nss-SO_4^{2-}] + [NO_3^-] + [NH_4^+] + [dust] + [non~dust]

see https://gricad-gitlab.univ-grenoble-alpes.fr/pmall/apli_pmall/-/issues/3 for details.

.. code-block:: python

   pyANDRA.plot_annual_mass_balance(data=df, fraction="PM10")

.. image:: ../figures/annual_mass_balance_PM10.png
   :width: 600px
   :align: center


Yearly concentration
~~~~~~~~~~~~~~~~~~~~

Plot a boxplot of the given species concentrations for the 2 size fractions:

.. code-block:: python

   pyANDRA.plot_yearly_dispersion(data=df, species=["Al", "Fe"])

.. image:: ../figures/yearly_dispersion_Al-Fe.png
   :width: 700px
   :align: center

Correlation heatmap
~~~~~~~~~~~~~~~~~~~

Heatmap of the Spearman coefficient for the given species and fraction:

.. code-block:: python

   elems = pyANDRA.IONS + pyANDRA.MONOSACH + pyANDRA.ETMS
   pyANDRA.plot_correlation(data=df, elems=elems, fraction="PM10")

.. image:: ../figures/correlation_PM10.png
   :width: 800px
   :align: center

Blanks values
~~~~~~~~~~~~~

Barplot of the mean and standard deviation of blanks :

.. code-block:: python

   pyANDRA.plot_blank(data=dfb, species=["Al", "Ca", "Fe", "K", "Na"], fraction="PM10")

.. image:: ../figures/blank_PM10_Al-Ca-Fe-K-Na.png
   :width: 800px
   :align: center


Blanks values per fractions
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Barplot of the mean and standard deviation of blanks for both fractions (PM10 and PM2.5):

.. code-block:: python

   pyANDRA.plot_blank_hue_fraction(data=dfb, species=["NO3-", "SO42-", "NH4+"])

.. image:: ../figures/blank_by_particle_size_NO3--SO42--NH4+.png
   :width: 800px
   :align: center


Tendancies
~~~~~~~~~~

This part use the STL deconvolution to estimate a tendancies of a given specie:

.. code-block:: python

   pyANDRA.plot_tendancies(data=df, specie="EC")


.. image:: ../figures/STL_EC_PM10.png
   :width: 800px
   :align: center

Module documentation
====================

Here are the signature of all function define in this module.

.. automodule:: pyANDRA
   :members:
   :undoc-members:
   :show-inheritance:

