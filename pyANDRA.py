import os
import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.ticker as mticker
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import seaborn as sns
import statsmodels.formula.api as smf
from statsmodels.tsa.seasonal import STL
from py4pm.chemutilities import pretty_specie, specie_unit, compute_PMreconstructed

mpl.rcParams['figure.dpi'] = 90
mpl.rcParams['axes.grid'] = True
mpl.rcParams['axes.axisbelow'] = True
mpl.rcParams['axes.labelsize'] = "large"

DB_EXCEL = "./BdD_ANDRA.xlsx"

DATE_MIN = "2012-01-01"
DATE_MAX = "2020-12-31"

DATE_MIN_AX = pd.to_datetime(DATE_MIN) - pd.DateOffset(months=3)
DATE_MAX_AX = pd.to_datetime(DATE_MAX) + pd.DateOffset(months=3)

SPECIES = [
        "OC", "EC", "NO3-", "SO42-", "NH4+", "Levoglucosan", "MSA"
        ]

ETMS = [
        "Al", "As", "Cd", "Cu", "Fe", "Mn", "Mo", "Ni", "Pb", "Rb", "Sb", "Se", "Ti", "V",
        "Zn"
    ]

IONS = ['Cl-', 'NO3-', 'SO42-', 'Na+', 'NH4+', 'K+', 'Mg2+', 'Ca2+']
MONOSACH = ['Levoglucosan', 'Mannosan']

NUMERIC_DATA = [
        'OC', 'EC', 'TC',
        'Cellulose',
        'MSA',
        'Cl-', 'NO3-', 'SO42-', 'Oxalate', 'Na+', 'NH4+', 'K+', 'Mg2+', 'Ca2+',
        'Br-', 'NO2-',
        'Inositol', 'Glycerol', 'Erythriol', 'Xylitol',
        'Arabitol', 'Sorbitol', 'Mannitol',
        'Threalose',
        'Levoglucosan', 'Mannosan', 'Galactosan',
        'Rhamnose', 'Glucose',
        'Malic_acid', 'Malonic_acid', 'Succinic_acid', 'Glutaric_acid',
        'Al', 'As', 'Ba', 'Ca', 'Cd', 'Ce', 'Co', 'Cr', 'Cs', 'Cu', 'Fe', 'K', 'La', 'Li',
        'Mg', 'Mn', 'Mo', 'Na', 'Ni', 'Pb', 'Pd', 'Pt', 'Rb', 'Sb', 'Sc', 'Se', 'Sn', 'Sr',
        'Ti', 'Tl', 'V', 'Zn', 'Zr'
    ]

def _savefig(filename):
    if not os.path.isdir("./figures"):
        os.mkdir("./figures")

    plt.savefig(
            f"./figures/{filename}",
            )

def format_xticks_as_date(ax):
    ax.set_xlim(
            pd.to_datetime(DATE_MIN_AX).to_pydatetime(),
            pd.to_datetime(DATE_MAX_AX).to_pydatetime()
            )
    ax.xaxis.set_major_locator(mdates.YearLocator())
    ax.xaxis.set_minor_locator(mdates.MonthLocator(bymonth=[7]))
    ax.xaxis.set_major_formatter(mdates.DateFormatter("%b\n%Y"))
    ax.xaxis.set_minor_formatter(mdates.DateFormatter("%b"))
    for label in ax.get_xticklabels():
        label.set_horizontalalignment('center')

# =====================================================================================
# ========= GET DATA PART
# =====================================================================================
def capitalize_and_strip_columns(df):
    """Rename the column with to their capitalized form
    syringic acid → Syringic acid

    Parameters
    ==========

    df : pd.DataFrame

    """
    df.columns = [
        x[:1].upper() + x[1:]
        if ((type(x) == str) and (x[:1] != "δ")) else x
        for x in df.columns.str.strip().str.replace(" ", "_")
    ]

def get_data_from_excel_sheet(filename, sheet_name="ANDRA-PM10"):
    """Read data from one excel sheet

    Parameters
    ==========

    filename : str
        Chemin d'accès au fichier excel
    sheet_name : str
        Feuille de calcul à utiliser

    Returns
    =======

    dfheader : pd.DataFrame
        Header containing the QL
    dfblank : pd.DataFrame
        All blank values
    dfval : pd.DataFrame
        Actual samples
    """
    df = pd.read_excel(
            filename,
            na_values=[
                "None", "n.a", "n.a.", "-", "Plus de surface pour etraction",
                "Echantillon non trouvé"],
            sheet_name=[sheet_name],
            parse_dates=["Date"],
        )[sheet_name]


    df = df.dropna(how='all').dropna(axis=1, how='all')
    df["Date"] = pd.to_datetime(df["Date"])
    df["Year"] = df["Date"].apply(lambda x: x.year)
    df["Month"] = df["Date"].apply(lambda x: x.month)

    # Separate the dataframe in header (i.e. QL value) and samples
    is_header = df["Date"].isnull()
    dfheader  = df.loc[is_header]
    dfsample  = df.loc[~is_header]
    
    # Get only date of interest, and remove unused columns
    dfsample = (dfsample
            .loc[(DATE_MIN < df["Date"]) & (df["Date"] < DATE_MAX) ]
            .dropna(axis=1, how="all")
            )

    # Split dataframe in blanks and values
    is_blank  = dfsample["Blank"].astype(bool)
    dfblank   = dfsample.loc[is_blank].replace({"<LQ": 0, "<QL": 0, "<DL": 0, "<LD": 0})
    dfval     = dfsample.loc[~is_blank]

    for df in [dfblank, dfval]:
        df["Year"] = df["Year"].apply(int)
        df["Month"] = df["Month"].apply(int)

    # Avoid column with space and non-capitalized first letter
    # and set index
    for df in [dfblank, dfval, dfheader]:
        capitalize_and_strip_columns(df)
        df.set_index(["Date"], inplace=True)

    # Replace <LD/<QL by the QL/2 of each specie for each year
    species = list(set(NUMERIC_DATA) & set(dfval.columns))
    year_min = pd.to_datetime(DATE_MIN).year
    year_max = pd.to_datetime(DATE_MAX).year
    for specie in species:
        for year in range(year_min, year_max+1):
            halfQL = dfblank.loc[dfblank["Year"]==year, specie].mean()/2
            replaced = dfval.loc[dfval["Year"]==year, specie].replace(
                    {
                        "<QL": halfQL,
                        "<LQ": halfQL,
                        "<LD": halfQL,
                        "<DL": halfQL,
                        },
                    )
            dfval.loc[dfval["Year"]==year, specie] = replaced

    dfval[species] = dfval[species].astype(float)
    dfval = dfval.loc[dfval["Volume"] >= 150 ]
    dfval["Polyols"] = dfval["Arabitol"] + dfval["Mannitol"]
    # dfval["Hopanes"] = dfval[['HP{}'.format(i) for i in range(1, 11)]].sum(axis=1)

    return (dfheader, dfblank, dfval)

def get_data_from_excel(filename, sheet_names=None):
    """Read and aggregate data from several  excel sheets

    Parameters
    ==========

    filename : str
        Le nom du fichier excel
    sheet_names : list
        Les feuilles à utiliser

    Returns
    =======

    dfheader : pd.DataFrame
        Header containing the QL
    dfblank : pd.DataFrame
        All blank values
    dfval : pd.DataFrame
        Actual samples
    """

    if sheet_names is None:
        sheet_names = ["ANDRA-PM10", "ANDRA-PM2.5"]

    dict_header = {}
    dict_blank = {}
    dict_val = {}
    for sheet_name in sheet_names:
        dftmpheader, dftmpblank, dftmpval = get_data_from_excel_sheet(filename, sheet_name=sheet_name)
        dict_header[sheet_name] = dftmpheader
        dict_blank[sheet_name] = dftmpblank
        dict_val[sheet_name] = dftmpval
    dfheader = pd.concat(dict_header, names=["Station"]).reset_index()
    dfblank = pd.concat(dict_blank, names=["Station"]).reset_index("Station")
    dfval = pd.concat(dict_val, names=["Station"]).reset_index("Station")

    return (dfheader, dfblank, dfval)


# =====================================================================================
# ========= PLOT PART
# =====================================================================================
def plot_annual_mass_balance(data, fraction="PM10", savefig=False):
    """Bilan de masse annuels moyen des PMx

    Le bilan de masse est calculé selon l'équation définie ici : 
    https://gricad-gitlab.univ-grenoble-alpes.fr/pmall/apli_pmall/-/issues/3

    Parameters
    ==========

    data : pd.DataFrame
        Les données
    fraction : str, PM10
        La fraction de taille à utiliser
    savefig : boolean, False
        Sauvegarder la figure en png.
    """
    df = compute_PMreconstructed(data.loc[data["Particle_size"] == fraction],
            takeOnlyPM10=False)

    df = df.groupby(pd.Grouper(freq="Y")).mean()

    df.index = df.index.year

    df = df.reindex(
            ["OM", "EC", "NO3-", "nss-SO42-", "NH4+", "seasalt", "dust", "nondust"],
            axis=1
            )

    fig, ax = plt.subplots(figsize=(8, 5))

    df.plot(
            kind="bar",
            ax=ax,
            stacked=True,
            color=[
                "green",
                "black",
                "darkblue",
                "darkred",
                "orange",
                "aquamarine",
                "lightblue",
                "salmon",
                "lightgrey"
                ]
            )

    ax.legend(loc="upper left", bbox_to_anchor=(1.05, 1))
    ax.set_ylabel("Concentration (µg.m⁻³)")
    ax.set_xlabel("")
    for t in ax.get_xticklabels():
        t.set_rotation(0)

    fig.subplots_adjust(
        right=0.8
        )
    ax.set_ylim(0, 13)

    ax.set_title(f"Concentration annuelle moyenne des {fraction}")
    
    if savefig:
        _savefig(f"annual_mass_balance_{fraction}.png")

def plot_timeseries(data, species, fraction="PM10", subplots=False, savefig=False):
    """Évolutions temporelles des espèces pour la fraction données.

    Parameters
    ==========

    data : pd.DataFrame
        Les données
    species : list
        Les espèces à utiliser
    fraction : str, PM10
        La fraction de taille à utiliser
    subplots : boolean, False
        Trace un subplot par espèce
    savefig : boolean, False
        Sauvegarder ou non la figure en png.
    """

    df = data.loc[data["Particle_size"]==fraction]

    plot_kws = dict(rot=0)

    if subplots:
        height = 2 + 2*len(species)
        nrows = len(species)
        plot_kws.update(
                dict(
                    subplots=True,
                    )
                )
    else:
        height = 4
        nrows = 1

    axes = df[species].plot(
            figsize=(12, height),
            **plot_kws
            )
    fig = plt.gcf()

    if subplots:
        for ax, specie in zip(fig.axes, species):
            h, l = ax.get_legend_handles_labels()
            l = [pretty_specie(specie)]
            ax.legend(h, l)
            if specie in ["OC", "EC"]:
                ylabels = "µg.m⁻³"
            else:
                ylabels = "ng.m⁻³"
            ax.set_ylabel(ylabels)
            ax.set_xlabel("")
        fig.subplots_adjust(
                top=0.925,
                bottom=0.11,
                left=0.11,
                right=0.9,
                hspace=0.2,
                wspace=0.2
        )
    else:
        ax = axes
        h, l = ax.get_legend_handles_labels()
        l = [pretty_specie(t) for t in species]
        ax.legend(h, l)
        if species[0] in ["OC", "EC"]:
            ylabels = "µg.m⁻³"
        else:
            ylabels = "ng.m⁻³"
        ax.set_ylabel(ylabels)
        ax.set_xlabel("")

    fig.suptitle(
            "Évolution temporelle du {species} (fraction {fraction})".format(
                species=", ".join([pretty_specie(s) for s in species]),
                fraction=fraction
                )
            )

    format_xticks_as_date(ax=ax)

    if savefig:
        _savefig(
            "timeserie_{species}_{subplot}{fraction}.png".format(
                species='-'.join(species),
                fraction=fraction,
                subplot="subploted_" if subplots else ""
                )
            )
    
def plot_timeseries_both_fraction(data, specie, savefig=False):
    """Évolutions temporelles de l'espèce donnée sur les 2 fractions.

    Parameters
    ==========
    data : pd.DataFrame
        Les données
    specie : str
        L'éspèce à utiliser
    savefig : boolean
        Sauvegarder ou non la figure en png.
    """

    df25 = data.loc[data["Particle_size"]=="PM2.5"]
    df10 = data.loc[data["Particle_size"]=="PM10"]

    if "/" in specie:
        sp1, sp2 = specie.split("/")
        df25[specie] = df25[sp1] / df25[sp2]
        df10[specie] = df10[sp1] / df10[sp2]

    fig, ax = plt.subplots(1, 1, figsize=(12, 5))

    plot_kws = dict(y=specie, rot=0, ax=ax)

    df25.plot(
            label="{} -- {}".format(*pretty_specie([specie, "PM2.5"])),
            **plot_kws
            )
    df10.plot(
            label="{} -- {}".format(*pretty_specie([specie, "PM10"])),
            **plot_kws
            )

    h, l = ax.get_legend_handles_labels()
    ax.legend(h, l)
    if specie in ["OC", "EC"]:
        ylabels = "µg.m⁻³"
    elif "/" in specie:
        ylabels = "ratio"
    else:
        ylabels = "ng.m⁻³"
    ax.set_ylabel(ylabels)
    ax.set_xlabel("")

    ax.axhline(0, ls="-", color="lightgrey")

    fig.suptitle(
            "Évolution temporelle du {specie} (PM10 et PM2.5)".format(
                specie=pretty_specie(specie),
                )
            )

    format_xticks_as_date(ax=ax)

    if savefig:
        _savefig(
            "timeserie_{specie}_PM10-PM25.png".format(
                specie=specie.replace("/", "-"),
                )
            )

def plot_timeseries_ratio(data, sp1, sp2, fractions=None, aggregate=None, savefig=False):
    """Évolutions temporelles du ratio de 2 espèces.

    Une figure à 2 axes est produite sur plusieurs fractions sont données.

    Parameters
    ==========

    data : pd.DataFrame
        Les données
    sp1 : str
        Espèce au nominateur
    sp2 : str
        Espèce au dénominateur
    fractions : list, ["PM10"]
        Fraction de taille à utiliser
    aggregate : str, None
        Aggrège les données en moyennes selon le pas de temps donnée ("M", "Q", etc).
    savefig : boolean
        Sauvegarder ou non la figure en png.
    """

    if fractions is None:
        fractions = ["PM10"]

    df = data.loc[data["Particle_size"].isin(fractions)]

    fig, axes = plt.subplots(
            figsize=(12, 2+len(fractions)*2),
            nrows=len(fractions),
            sharex=True,
            )

    if len(fractions) == 1:
        axes = [axes]

    for fraction, ax in zip(fractions, axes):
        dftmp = df.loc[df["Particle_size"] == fraction]

        ratio = dftmp[sp1] / dftmp[sp2]

        if aggregate:
            ratio = ratio.resample(aggregate).mean()

        ratio.plot(
            ax=ax,
            x_compat=True,
            rot=0
        )

        h, l = ax.get_legend_handles_labels()
        l = f"{pretty_specie(sp1)}/{pretty_specie(sp2)}"
        ax.legend(h, [f"{sp1}/{sp2}"])

        ax.set_ylabel(f"ratio {sp1}/{sp2}")
        ax.set_xlabel("")
        ax.set_title(
                "Évolution temporelle du ratio {sp1}/{sp2} (fraction {fraction})".format(
                    sp1=pretty_specie(sp1),
                    sp2=pretty_specie(sp2),
                    fraction=fraction
                    )
                )

        format_xticks_as_date(ax=ax)

        ax.axhline(0, ls="--", color="lightgrey")
    
    if len(fractions) > 1:
        fig.subplots_adjust(
                top=0.9,
                bottom=0.1,
                hspace=0.25
                )
    if savefig:
        _savefig(
            "timeserie_ratio_{sp1}-{sp2}_{fractions}{aggregate}.png".format(
                sp1=sp1,
                sp2=sp2,
                fractions="-".join(fractions),
                aggregate=f"_{aggregate}" if aggregate else ""
                )
            )
    return ratio

def plot_yearly_dispersion(data, species, savefig=False):
    """Barplot (moyenne±std) de la concentration annuelles des espèces pour les PM10 et
    PM2.5

    Parameters
    ==========

    data : pd.DataFrame
        Les données
    species : list
        Les espèces à utiliser
    savefig : boolean, False
        Sauvegarder ou non la figure en png.
    """
    
    df = data[species + ["Particle_size"]]
    df["year"] = df.index.year

    df = df.melt(
            id_vars=["year", "Particle_size"],
            var_name="specie",
            value_name="concentration"
            )

    dfb25 = df.loc[df["Particle_size"] == "PM2.5"]
    dfb10 = df.loc[df["Particle_size"] == "PM10"]

    fig, axes = plt.subplots(1, 2, figsize=(12, 6), sharey=True)
    sns.boxplot(
            data=dfb25,
            x="specie",
            y="concentration",
            hue="year",
            palette="viridis",
            ax=axes[0],
            showfliers=False
            )
    sns.boxplot(
            data=dfb10,
            x="specie",
            y="concentration",
            hue="year",
            palette="viridis",
            ax=axes[1],
            showfliers=False
            )

    axes[0].legend().remove()
    axes[0].set_ylabel(
            "Concentration en {}".format(specie_unit(species[0])),
            )
    axes[0].set_ylim(bottom=0)
    axes[1].set_ylabel("")
    axes[1].legend(bbox_to_anchor=(1,1), loc="upper left", frameon=False)

    for ax, PM in zip(axes, ["PM2.5", "PM10"]):
        ax.text(
                0.01, 0.95,
                pretty_specie(PM),
                fontsize=14,
                fontweight="bold",
                transform=ax.transAxes
                )
        ax.set_xticklabels([pretty_specie(x.get_text()) for x in ax.get_xticklabels()],
            fontsize=12)
        plt.setp(ax.get_yticklabels(), fontsize=12)
        ax.set_xlabel("")

    fig.subplots_adjust(wspace=0)
    if savefig:
        _savefig(
            "yearly_dispersion_{}.png".format("-".join(species))
            )

def plot_correlation(data, elems, fraction="PM10", savefig=False):
    """Matrice de corrélation

    Parameters
    ==========

    data : pd.DataFrame
        Les données
    elems : list
        Listes des espèces à utiliser
    fraction : str, PM10
        Fraction de taille à utiliser
    savefig : boolean
        Sauvegarder ou non la figure en png.
    """

    df = data.loc[data["Particle_size"] == fraction]

    corr = df[elems].corr("spearman")

    mask = np.zeros_like(corr)
    mask[np.triu_indices_from(mask)] = True

    fig = plt.figure(figsize=(len(elems)/2+1,8))
    sns.heatmap(
            corr,
            vmin=-1,
            vmax=1,
            cmap="RdBu_r",
            annot=True,
            fmt=".2f",
            mask=mask
            )
    ax = plt.gca()
    ax.set_title(f"Corrélation temporelle (spearman)\n({DATE_MIN} à {DATE_MAX}, fraction {fraction})")

    fig.subplots_adjust(top=0.925, bottom=0.15, right=1)

    if savefig:
        _savefig(f"correlation_{fraction}.png")

def plot_ratio_PM10_PM25(data, sp, aggregate="M", estimator="mean", savefig=False):
    """Trace la série temporelle du ratio PM10/PM2.5 de l'espèce considérée.

    Parameters
    ==========

    data : pd.DataFrame
        Les données
    sp : str
        L'espèce en question
    aggregate : str, M
        Pas de temps de l'aggrégation (mensuelle par défaut)
    estimator : str, mean
        Méthode d'aggrégation (moyenne par défaut)
    savefig : boolean
        Sauvegarder ou non la figure en png.

    """
    PM10 = (
            data.loc[data["Particle_size"] == "PM10", :]
            .resample(aggregate)
            .mean()
            )
    PM25 = (
            data.loc[data["Particle_size"] == "PM2.5", :]
            .resample(aggregate)
            .mean()
            )

    count = (
            data.loc[data["Particle_size"] == "PM2.5", :]
            .resample(aggregate)
            .count()
            )
    fig, ax = plt.subplots(figsize=(12, 4))

    ratio = PM10[sp] / PM25[sp]
    ratio.plot(
        ax=ax,
        x_compat=True,
        rot=0
    )

    h, l = ax.get_legend_handles_labels()
    l = [f"{sp} PM$_{{10}}$ / {sp} PM$_{{2.5}}$"]
    ax.legend(h, l, loc="upper right")

    ax.set_ylabel(f"ratio {sp} PM$_{{10}}$ / PM$_{{2.5}}$")
    ax.set_xlabel("")
    ax.set_title(
            "Évolution temporelle du ratio {sp} PM$_{{10}}$ / PM$_{{2.5}}$".format(
                sp=sp
                )
            )

    format_xticks_as_date(ax=ax)

    ax.set_ylim(-0.3, 3.0)
    ax.axhline(0, ls="--", color="lightgrey")
    
    if savefig:
        _savefig(
            "timeserie_ratio_{sp}_PM10_PM25.png".format(
                sp=sp,
                )
            )

    # MONTHLY

    ratio = ratio.to_frame()
    ratio["month"] = ratio.index.month
    fig, ax = plt.subplots(figsize=(8, 4))
    sns.barplot(data=ratio, x="month", y=sp, ci="sd", color="lightgrey")
    ax.set_title(f"Cycle saisonnier du ratio {sp} PM$_{{10}}$ / PM$_{{2.5}}$ (moyenne et écart type)")
    ax.set_ylabel(f"ratio {sp} PM$_{{10}}$ / PM$_{{2.5}}$")
    ax.set_xlabel("Mois")
    if savefig:
        _savefig(
            "timeserie_ratio_{sp}_PM10_PM25_seasonal_cycle.png".format(
                sp=sp,
                )
            )


    # COUNT
    fig, ax = plt.subplots(figsize=(12, 4))
    count[sp].plot(ax=ax, rot=0, x_compat=True)
    ax.set_ylabel(f"# of samples")
    ax.set_xlabel("")
    ax.set_title(
            "Nombre d'échantillon aggrégés"
            )

    format_xticks_as_date(ax=ax)
    ax.set_ylim(0, 9)

    if savefig:
        _savefig(
            "timeserie_ratio_count_PM10_PM25.png"
            )

def plot_blank(data, species=None, fraction="PM10", savefig=False):
    """Trace les blanc (moyenne±écart type) pour une fraction de taille donnée.

    Parameters
    ==========
    
    df : pd.DataFrame
        Les données des blancs, avec une colonne `Particle_size`
    species : list
        Les espèces à tracer
    fraction : str, PM10
        Fraction de taille à considérer (PM10 ou PM2.5)
    savefig : boolean, False
        Sauvegarder la figure
    """

    if species is None:
        species = ["OC", "EC"]

    dfb = data.loc[data["Particle_size"]==fraction]

    dfb["year"] = dfb.index.year

    dfb = dfb[species + ["year"]]

    dfb = dfb.melt(
            id_vars="year",
            var_name="specie",
            value_name="blank_value"
            )

    fig, ax = plt.subplots(1, 1, figsize=(12, 6))
    sns.barplot(
            data=dfb,
            x="specie",
            y="blank_value",
            hue="year",
            palette="viridis",
            ci="sd",
            ax=ax
            )

    ax.legend(bbox_to_anchor=(1,1), loc="upper left", frameon=False)
    ax.set_ylabel(
            "Concentration des blancs\nmoyenne±écart type en ng m⁻³",
            )
    ax.set_xticklabels([pretty_specie(x.get_text()) for x in ax.get_xticklabels()],
            fontsize=12)
    plt.setp(ax.get_yticklabels(), fontsize=12)
    ax.set_xlabel("")
    ax.set_ylim(bottom=0)
    ax.text(0.01, 0.95, pretty_specie(fraction),
            fontsize=14,
            fontweight="bold",
            transform=ax.transAxes
            )


    if savefig:
        _savefig(
            "blank_{}_{}.png".format(fraction, "-".join(species))
            )

def plot_blank_hue_fraction(df, species=None, savefig=False):
    """Trace les blancs (moyenne±écart-type) sur 2 axes (PM10 et PM2.5)

    Parameters
    ==========
    
    df : pd.DataFrame
        Les données des blancs, avec une colonne `Particle_size`
    species : list
        Les espèces à tracer
    savefig : boolean, False
        Sauvegarder la figure
    """

    dfb = df[species + ["Particle_size"]]
    dfb["year"] = dfb.index.year

    dfb = dfb.melt(
            id_vars=["year", "Particle_size"],
            var_name="specie",
            value_name="blank_value"
            )

    dfb25 = dfb.loc[dfb["Particle_size"] == "PM2.5"]
    dfb10 = dfb.loc[dfb["Particle_size"] == "PM10"]
    fig, axes = plt.subplots(1, 2, figsize=(12, 6), sharey=True)
    sns.barplot(
            data=dfb25,
            x="specie",
            y="blank_value",
            hue="year",
            palette="viridis",
            ci="sd",
            ax=axes[0]
            )
    sns.barplot(
            data=dfb10,
            x="specie",
            y="blank_value",
            hue="year",
            palette="viridis",
            ci="sd",
            ax=axes[1]
            )

    axes[0].legend().remove()
    axes[0].set_ylabel(
            "Concentration des blancs\nmoyenne±écart type en {}".format(specie_unit(species[0])),
            )
    axes[0].set_ylim(bottom=0)
    axes[1].set_ylabel("")
    axes[1].legend(bbox_to_anchor=(1,1), loc="upper left", frameon=False)

    for ax, PM in zip(axes, ["PM2.5", "PM10"]):
        ax.text(
                0.01, 0.95,
                pretty_specie(PM),
                fontsize=14,
                fontweight="bold",
                transform=ax.transAxes
                )
        ax.set_xticklabels([pretty_specie(x.get_text()) for x in ax.get_xticklabels()],
            fontsize=12)
        plt.setp(ax.get_yticklabels(), fontsize=12)
        ax.set_xlabel("")

    fig.subplots_adjust(wspace=0)
    if savefig:
        _savefig(
            "blank_by_particle_size_{}.png".format("-".join(species))
            )

def plot_tendancies(data, specie, fraction="PM10", savefig=False):
    """Trace la déconvolution STL des espèces données, avec la tendances par an.

    La déconvolution STL utilises les moyennes mensuelles et est faite avec statsmodels.
    L'options `robust`, qui diminue le poids des événements extremes.
    Si des valeurs sont manquantes, un warning est émis et la valeur manquante est estimé
    par back-filling.


    Parameters
    ==========

    df : pd.DataFrame
        Les données
    species : list
        Les espèces à tracer
    fraction : str
        PM10 ou PM2.5
    savefig : boolean
        Sauvegarder les figures en png
    """
    
    df = data.loc[data["Particle_size"] == fraction, specie]
    df = df.resample("M").mean()

    if df.isnull().any():
        print(f"Warning: filling NaN value for {specie} (#={data.isnull().sum()})")
        data = data.fillna(method="bfill")

    model = STL(df, robust=True)
    reg = model.fit()

    data_fit = reg.trend.copy().to_frame()
    data_fit["x"] = range(0, len(reg.trend))
    linfit = smf.ols('trend~x', data=data_fit).fit()
    a = linfit.params[1]
    b = linfit.params[0]

    fig, axes = plt.subplots(figsize=(12,7), nrows=4, ncols=1, sharex=True,
            sharey=False)

    axes[0].plot(reg.observed, label="Moyenne mensuelle")
    axes[1].plot(reg.trend, label="Tendance")
    axes[2].plot(reg.seasonal, label="Saisonnalité")
    axes[3].plot(reg.resid, label="Résidus")
    
    xmin, xmax = reg.trend.index[0], reg.trend.index[-1]
    axes[1].plot([xmin, xmax], [b, a*len(reg.trend) + b], label="Fit tendance")

    for ax in axes:
        ax.legend(loc="upper right")

    for ax in [axes[0], axes[1]]:
        ax.set_ylim(bottom=0)

    axes[0].set_ylabel("Concentration\n({})".format(specie_unit(specie)))
    axes[1].set_ylabel("Tendance\n({})".format(specie_unit(specie)))
    axes[2].set_ylabel("Saisonnalité")
    axes[3].set_ylabel("Résidus")

    axes[1].annotate(
            "y={a:.3f}x + {b:.3f} (r²={r2:.2f}), soit {a_an:.3f} {unit} an⁻¹".format(
                a=a,
                b=b,
                r2=linfit.rsquared,
                a_an=a*12,
                unit=specie_unit(specie)
                ),
            xy=(0.05, 0.1),
            xycoords="axes fraction",
            fontsize=12,
            backgroundcolor="white"
            )

    fig.subplots_adjust(bottom=0.1, top=0.9)
    fig.suptitle(f"Déconvolution STL de {specie} sur les {fraction}")

    if savefig:
        _savefig(f"STL_{specie}_{fraction}.png")


# =====================================================================================
# ========= Main fonction to run everything
# =====================================================================================
def plot_all(data, dfblank, fractions=["PM10", "PM2.5"], savefig=False):
    """Plot the required figures

    Parameters
    ==========
    
    data : pd.DataFrame
        Données, avec les PM10 et PM2.5
    dfblank : pd.DataFrame
        Données des blancs
    fractions : list of str
        List of particle size to use (default ["PM10", "PM2.5"])
    savefig : boolean
        Save or not the figures as png

    Returns
    =======

    Nothing
    """

    # ==== blank part
    for species in [["OC"], ["NO3-", "SO42-", "NH4+"]]:
        plot_blank_hue_fraction(dfblank, species=species, savefig=savefig)

    to_plot = [
            ["Cl-", "Oxalate", "Na+", "K+", "Mg2+", "Ca2+"],
            ["Al", "Fe"],
            ["Cu", "Mn", "Ni", "Ti", "Zn"],
            ["Al", "Ca", "Fe", "K", "Na"],
            ["Cd", "Cu", "Mn", "Ni", "Ti", "Zn", "Zr"],
            ["HP{}".format(i) for i in range(1,11)]
            ]
    for fraction in fractions:
        for species in to_plot:
            plot_blank(dfblank, species=species, fraction=fraction, savefig=savefig)

    # ==== Barplot of annual mass balance (mean)
    for fraction in fractions:
        plot_annual_mass_balance(data, fraction=fraction, savefig=savefig)

    # ==== Raw timeseries of species. If it's a list of species, then they are subploted
    # (i.e. one per row)
    species = [
            ["OC", "EC"], 
            ["NO3-", "SO42-", "NH4+"], 
            ["OC"],
            ["EC"],
            ["NO3-"],
            ["NH4+"],
            ["SO42-"],
            ["Levoglucosan"],
            ["MSA"],
            ["Polyols"],
            ["MSA", "Polyols"]
            # ["Benzo(a)pyrene", "EC"],
            # ["Hopanes"]
    ]
    for fraction in ["PM10", "PM2.5"]:
        for sp in species:
            subploted = True if len(sp)>1 else False
            plot_timeseries(data, species=sp, fraction=fraction, savefig=savefig, subplots=subploted)
    
    # ==== Timeserie with both PM10 and PM2.5
    to_plot = ["OC", "EC", "EC/OC", "Levoglucosan", "Mannosan", ]
    for sp in to_plot:
        plot_timeseries_both_fraction(data, specie=sp, savefig=savefig)

    # ==== Timeserie of ratio of sp1 over sp2
    for sp1, sp2 in [["EC", "OC"]]:
        plot_timeseries_ratio(data, sp1=sp1, sp2=sp2, fractions=["PM10"], savefig=savefig)

    # ==== Correlation matrix of Trace element
    # plot_correlation(data, ETMS, fraction="PM10", savefig=savefig)
    # ==== Correlation matrix of lots of stuff
    plot_correlation(data, IONS+MONOSACH+ETMS, fraction="PM10", savefig=savefig)

    #====  Ratio of species between PM10 and PM2.5
    # Produce 3 plots : timeseries, monthly and count of samples
    for sp in ["OC", "EC"]:
        plot_ratio_PM10_PM25(data, sp, aggregate="M", estimator="mean", savefig=savefig)

    # ==== Yearly boxplot (without fliers)
    to_plot = [
            ["Al", "Fe"],
            ["As", "Cd"],
            ["V", "Ni"],
            ["Ti", "Cu", "Pb"]
            ]
    for species in to_plot:
        plot_yearly_dispersion(data, species=species, savefig=savefig)

    # ==== Tendancies
    to_plot = ["EC", "OC", "Cu"]
    for specie in to_plot:
        plot_tendancies(data=data, specie=specie, fraction="PM10", savefig=savefig)

if __name__ == "__main__":
    dfh, dfb, df = get_data_from_excel(DB_EXCEL, sheet_names=["ANDRA-PM10", "ANDRA-PM2.5"])

    plt.interactive(False)
    plot_all(data=df, dfblank=dfb, fractions=["PM10", "PM2.5"], savefig=True)
    plt.interactive(True)

    print("All Done! Check your ./figures folders now.")
